#include "utils.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv)
{

  int socket_desc, new_socket, valread;
  struct sockaddr_in server_address;
  int address_len;
  char buffer[REPLY_SIZE] = {0};
  char socket_response[REPLY_SIZE] = {0};
  char input[LINE_SIZE] = {0};

  // Creating socket file descripton
  if ((socket_desc = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    exit_msg("Socket failed.", 0);
  }

  server_address.sin_family = AF_INET;
  server_address.sin_addr.s_addr = INADDR_ANY;
  server_address.sin_port = htons(SERVER_PORT);
  address_len = sizeof(server_address);

  if (connect(socket_desc, (struct sockaddr *)&server_address, address_len) < 0)
  {
    exit_msg("Client socket connexion failed.", 0);
  }
  printf("Client socket connected.\n");
  while (strcmp(input, "QUIT") != 0)
  {
    fgets(input, LINE_SIZE, stdin);
    send(socket_desc, input, LINE_SIZE, 0);
    valread = read(socket_desc, socket_response, REPLY_SIZE);
    printf("%s\n", socket_response);
  }
  close(socket_desc);
  return 0;
}

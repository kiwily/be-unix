#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "bdd.h"
#include "utils.h"

//Nom du fichier contenant les données
static const char *DATA = "data";

//Retourne une string à partir d'un Day
char *day_to_string(enum Day d)
{
  switch (d)
  {
  case MON:
    return "Lundi";
  case TUE:
    return "Mardi";
  case WED:
    return "Mercredi";
  case THU:
    return "Jeudi";
  case FRI:
    return "Vendredi";
  case SAT:
    return "Samedi";
  case SUN:
    return "Dimanche";
  case NONE:
    return "Not a day";
  }
}

//Retourne un Day à partir d'un string
//dans le cas où la string ne correspond pas à un jour, on renvoie NONE
enum Day string_to_day(char *dd)
{
  char d[LINE_SIZE];
  strcpy(d, dd);
  //Conversion en minuscule
  for (int i = 0; i < strlen(d); i++)
    d[i] = tolower(d[i]);

  if (strcmp("lundi", d) == 0)
    return MON;
  else if (strcmp("mardi", d) == 0)
    return TUE;
  else if (strcmp("mercredi", d) == 0)
    return WED;
  else if (strcmp("jeudi", d) == 0)
    return THU;
  else if (strcmp("vendredi", d) == 0)
    return FRI;
  else if (strcmp("samedi", d) == 0)
    return SAT;
  else if (strcmp("dimanche", d) == 0)
    return SUN;
  else
    return NONE;
}

// Libère la mémoire d'un pointeur vers Data
void data_free(Data *d)
{
  free(d->name);
  free(d->activity);
  free(d);
}

//Modifie une chaîne de caratère correspondant à data
void data_format(char *l, Data *data)
{
  sprintf(l, "%s,%s,%s,%d\n",
          data->name, data->activity,
          day_to_string(data->day), data->hour);
}

// Retourne une structure Data à partir d'une ligne de donnée
// get_data("toto,arc,lundi,4") ->  Data { "toto", "arc", MON, 4 };
// Attention il faudra libérer la mémoire vous-même après avoir utilisé
// le pointeur généré par cette fonction
Data *get_data(char *line)
{
  char *parse;
  Data *data = malloc(sizeof(Data));
  char error_msg[LINE_SIZE];
  sprintf(error_msg, "Erreur de parsing pour: %s\n", line);

  // On s'assure que la ligne qu'on parse soit dans la mémoire autorisée en
  // écriture
  char *l = malloc(strlen(line) + 1);
  l = strncpy(l, line, strlen(line) + 1);

  parse = strtok(l, ",");
  if (parse == NULL)
    exit_msg(error_msg, 0);
  data->name = malloc(strlen(parse) + 1);
  strcpy(data->name, parse);

  parse = strtok(NULL, ",");
  if (parse == NULL)
    exit_msg(error_msg, 0);
  data->activity = malloc(strlen(parse) + 1);
  strcpy(data->activity, parse);

  parse = strtok(NULL, ",");
  if (parse == NULL)
    exit_msg(error_msg, 0);
  data->day = string_to_day(parse);

  parse = strtok(NULL, "\n");
  if (parse == NULL)
    exit_msg(error_msg, 0);
  data->hour = atoi(parse);
  free(l);

  return data;
}

// Formate une ligne x,y,z,t pour l'affichage.
void print_line(char *line)
{
  Data *data;
  data = get_data(line);
  printf("%s %dh : %s a %s.\n",
         day_to_string(data->day),
         data->hour,
         data->name,
         data->activity);
  data_free(data);
}

// La fonction _add_data_  retourne 0 si l'opération s'est bien déroulé
//sinon -1
int add_data(Data *data)
{
  FILE *fichier = NULL;

  fichier = fopen(DATA, "a");

  if (fichier != NULL)
  {
    char chaine[LINE_SIZE];
    data_format(chaine, data);
    fputs(chaine, fichier);

    fclose(fichier);
    printf("Commande effectuée.\n");
    return 0;
  }
  else
  {
    return -1;
  }
}

//Enlève la donnée _data_ de la base de donnée
int delete_data(Data *data)
{
  FILE *fichier = NULL;
  FILE *fichier_temp = NULL;
  char chaine[LINE_SIZE] = "";
  char chaine_data[LINE_SIZE] = "";
  bool done = false;
  data_format(chaine_data, data);

  fichier = fopen(DATA, "r");
  fichier_temp = fopen("temp", "w");

  if (fichier != NULL)
  {
    if (fichier_temp != NULL)
    {
      // Parcours complet du fichier data
      while (fgets(chaine, LINE_SIZE, fichier) != NULL)
      {
        // Si ce n'est pas la ligne data, on la copie dans le fichier temp
        if (strcmp(chaine, chaine_data) != 0)
        {
          fputs(chaine, fichier_temp);
        }
        else
        {
          done = true;
        }
      }
      fclose(fichier_temp);
      fclose(fichier);
    }
    else
    {
      return -1;
    }
  }
  else
  {
    return -1;
  }
  // Remplacement du fichier data par le fichier temporaire
  remove(DATA);
  rename("temp", DATA);
  if (done == false)
  {
    printf("Aucune ligne ne correspond à : %s", chaine_data);
  }
  printf("Commande effectuée.\n");
  return 0;
}

//Affiche le planning
void see_all()
{
  FILE *fichier = NULL;
  char chaine[LINE_SIZE] = "";

  fichier = fopen(DATA, "r");

  if (fichier != NULL)
  {
    printf("---------Planning---------\n");
    // Parcours complet du fichier data
    while (fgets(chaine, LINE_SIZE, fichier) != NULL)
    {
      print_line(chaine);
    }
    fclose(fichier);
  }
  /*else
  {
    return -1;
  }
  return 0;*/
}

int main(int argc, char **argv)
{
  /*printf("\ncalled bdd");
  for (int i = 0; i < argc; i++)
  {
    printf("\targ : %s\n", argv[i]);
  }*/
  if (argc == 2)
  {
    if (strcmp("SEE", argv[1]) == 0)
    {
      see_all();
    }
    else if (strcmp("MAN", argv[1]) == 0)
    {
      printf("\n \
--------------------manuel-------------------\n \
  Les commandes disponibles sont :\n \
      $ ADD <nom> <activité> <jour> <heure>\n \
      $ SEE\n \
      $ DEL <nom> <activité> <jour> <heure>\n \
      $ QUIT\n \
---------------------------------------------\n\n");
    }
    else if (strcmp("QUIT", argv[1]) != 0)
    {
      printf("Mauvaise commande. Utilisez MAN pour de l'aide.\n");
    }
  }
  else if (argc == 6)
  {
    char line[LINE_SIZE];
    sprintf(line, "%s,%s,%s,%s", argv[2], argv[3], argv[4], argv[5]);
    Data *data = get_data(line);
    if (strcmp("ADD", argv[1]) == 0)
    {
      return add_data(data);
    }
    else if (strcmp("DEL", argv[1]) == 0)
    {
      return delete_data(data);
    }
    else
    {
      printf("Mauvaise commande. Utilisez MAN pour de l'aide.\n");
    }
    data_free(data);
  }
  else
  {
    printf("Mauvaise commande. Utilisez MAN pour de l'aide.\n");
  }
  return 0;
}

#include <string.h>
#include "utils.h"
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <semaphore.h>
#include <pthread.h>

char *bdd_bin = "./bdd";
sem_t mutex;

//On prépare les arguments qui seront envoyés à bdd
//ADD toto poney lundi 3 -> { "./bdd", "ADD", "toto", "poney", lundi", "3", NULL }
char **parse(char *line)
{
  char **res = malloc(7 * sizeof(char *));
  res[0] = bdd_bin;

  char *arg1 = strtok(line, " ");
  res[1] = arg1;

  char *arg2 = strtok(NULL, " ");
  res[2] = arg2;
  if (arg2 == NULL)
  {
    arg1[strlen(arg1) - 1] = '\0';
    return res;
  }

  char *arg3 = strtok(NULL, " ");
  res[3] = arg3;
  if (arg3 == NULL)
  {
    arg2[strlen(arg2) - 1] = '\0';
    return res;
  }

  char *arg4 = strtok(NULL, " ");
  res[4] = arg4;
  if (arg4 == NULL)
  {
    arg3[strlen(arg3) - 1] = '\0';
    return res;
  }

  char *arg5 = strtok(NULL, "\n");
  res[5] = arg5;
  res[6] = NULL;
  return res;
}

//Configuration de la socket réseau, retourne le file descriptor de la socket
int configure_socket()
{
  int socket_desc;
  struct sockaddr_in address;
  int address_len;

  printf("\nconfigure server's socket");
  // Creating socket file descriptor
  if ((socket_desc = socket(AF_INET, SOCK_STREAM, 0)) == 0)
  {
    exit_msg("Socket failed.", 0);
  }

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(SERVER_PORT);
  address_len = sizeof(address);

  if (bind(socket_desc, (struct sockaddr *)&address,
           address_len) < 0)
  {
    exit_msg("Socket bind failed.", 0);
  }
  printf("\nconfigure server's socket");
  if (listen(socket_desc, 3) < 0)
  {
    exit_msg("Socket listening failed.", 0);
  }

  printf("\nserver's socket configured");
  return socket_desc;
}

// Passage des commandes à la base de données par un pipe
// Renvoi des réponses au client par la socket réseau
void *process_communication(void *n_veg)
{
  int socket_desc = *(int *)n_veg;
  int fds[2];

  while (1){
    char buffer[REPLY_SIZE] = {0};
    char piped_response[REPLY_SIZE] = {0};
    // Lecture des données envoyées par le client
    read(socket_desc, buffer, REPLY_SIZE);

    // Vérification des données envoyées
    if (strcmp(buffer, "QUIT") == 0)
    {
      exit_msg("Server terminated.", 0);
    } else {
      // Parsing des données du client
      char **args = parse(buffer);

      // Création du pipe et vérifications
      if (pipe(fds) == -1)
      {
        exit_msg("Pipe Failed", 0);
      }
      // Attente de la disponibilité du semaphore
      sem_wait(&mutex);

      // Duplication du processus et vérifications
      pid_t pid = fork();
      if (pid < 0) {
        exit_msg("Erreur de fork.", 0);
      }

      // Processus pere
      if (pid) {
        // Fermeture du coté écriture
        close(fds[1]);

        // Lecture du pipe (la réponse de la bdd)
        read(fds[0], piped_response, REPLY_SIZE);

        // Relache le mutex maintenant que la bdd a termine
        sem_post(&mutex);

        // Envoi des données de la bdd au client
        send(socket_desc, piped_response, REPLY_SIZE, 0);

        // On continue a ecouter grace a la boucle while

      } else {
        // Processus fils
        // Fermeture du coté lecture
        close(fds[0]);

        // Relier la sortie à l'écriture du pipe
        dup2(fds[1], STDOUT_FILENO);

        // Appel à la bdd (qui écrira dans la sortie et donc dans le pipe)
        execl(bdd_bin, args[0], args[1], args[2], args[3], args[4], args[5], args[6], NULL);
        // Execl va exit apres avoir execute les commandes de la bdd
      }
    }
  }
}

// Etablis une connexion lors d'une demande de connexion entrante
// Met en place l'écoute pour la réponse au client
void connect_socket(int socket_desc)
{
  int new_socket;
  struct sockaddr_in client_address;
  int address_len = sizeof(struct sockaddr_in);
  pthread_t threads[MAX_THREADS];
  int thread_num = 0;

  printf("attnouvelle co du socket i, creation d'un thread pour lui");
  while (thread_num < MAX_THREADS)
  {
    printf("attnouvelle co du socket i, creation d'un thread pour lui");
    // Attente de connexion
    new_socket = accept(socket_desc, (struct sockaddr *)&client_address,
                        (socklen_t *)&address_len);

    printf("nouvelle co du socket %i, creation d'un thread pour lui", socket_desc);
    // Vérification d'une connexion réussie
    if (new_socket < 0)
    {
      exit_msg("Socket accept failed.", 0);
    }

    printf("nouvelle co du socket %i, creation d'un thread pour lui", socket_desc);
    // Gestion des commandes entrantes dans la nouvelle socket dans un autre thread
    if (pthread_create(&threads[thread_num++], NULL, process_communication, &new_socket) != 0 ){
      printf("Failed to create thread\n");
    }
  }
}

int main(int argc, char **argv)
{
  int socket_desc;
  char command[LINE_SIZE] = "";
  //sem_init(&mutex, 0, 1);

  // Configuration de la socket serveur
  socket_desc = configure_socket();

  // Réception des connexions réseaux entrantes
  connect_socket(socket_desc);

  /*end hide*/
  return 0;
}

/*Dis j'ai une petite question, je suis en train de faire le tp BE Unix du cours de programmation système. Et là je me retrouve face à un petit souci : 
Je peux connecter plusieurs clients à mon server avec du multithreading, et chaque client peut envoyer une requete qui sera transmise à la bdd en dupliquant le processus et en envoyant*/
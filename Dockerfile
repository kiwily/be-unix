FROM ubuntu
RUN apt update
RUN apt install -y build-essential
RUN apt install -y libc6-dbg gdb valgrind

CMD ["/bin/bash"]
WORKDIR /usr/src/


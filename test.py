import itertools

# Jaune = 0
# Bleu = 1
# Rouge = 2
# Vert = 3

# Un carré c'est depuis en haut à gauche dans le sens des aiguilles d'une montre


# 1 arrangement c'est 1 liste de 8 carrés
# On rempli d'en haut à gauche, en haut à droite, on descend d'un étage gauche puis droite, ...
def test_arrangement(arrangement):
    carre_1 = arrangement[0]
    carre_2 = arrangement[1]
    carre_3 = arrangement[2]
    carre_4 = arrangement[3]
    carre_5 = arrangement[4]
    carre_6 = arrangement[5]
    carre_7 = arrangement[6]
    carre_8 = arrangement[7]

    result = True

    result = result and test_colle_horizontal(carre_1, carre_2)
    result = result and test_colle_horizontal(carre_3, carre_4)
    result = result and test_colle_horizontal(carre_5, carre_6)
    result = result and test_colle_horizontal(carre_7, carre_8)

    result = result and test_colle_vertical(carre_1, carre_3)
    result = result and test_colle_vertical(carre_2, carre_4)
    result = result and test_colle_vertical(carre_3, carre_5)
    result = result and test_colle_vertical(carre_4, carre_6)
    result = result and test_colle_vertical(carre_5, carre_7)
    result = result and test_colle_vertical(carre_6, carre_8)

    return result


# Carré 1 est à gauche, carré 2 à droite
def test_colle_horizontal(carre_1, carre_2):
    return carre_1[1] == carre_2[0] and carre_1[2] == carre_2[3]


# Carré 1 en haut, carré 2 en bas
def test_colle_vertical(carre_1, carre_2):
    return carre_1[3] == carre_2[0] and carre_1[2] == carre_2[1]


def turn(arrangement, carre_a_permuter):
    carre = arrangement[carre_a_permuter]
    new_carre = carre[1:]
    new_carre.append(carre[0])
    arrangement[carre_a_permuter] = new_carre


if __name__ == "__main__":
    c1 = [0, 2, 3, 1]
    c2 = [0, 1, 2, 3]
    c3 = [0, 1, 3, 2]
    c4 = [0, 2, 3, 1]
    c5 = [0, 2, 3, 1]
    c6 = [0, 3, 1, 2]
    c7 = [0, 3, 2, 1]
    c8 = [0, 1, 3, 2]

    arrangement = [c1, c2, c3, c4, c5, c6, c7, c8]

    not_ended = True
    permutations = list(itertools.permutations([0, 1, 2, 3, 4, 5, 6, 7]))
    j = 0
    for carre_a_permuter in range(8):
        if not_ended:
            print(
                "=" * 40,
                "changement de carré à permuter",
                carre_a_permuter,
                arrangement[carre_a_permuter],
            )
            for permutage in range(4):
                if not_ended:
                    turn(arrangement, carre_a_permuter)
                    print(
                        "." * 40,
                        "permutation du carré à permuter",
                        carre_a_permuter,
                        arrangement[carre_a_permuter],
                    )
                    for perm in permutations:
                        if not_ended:
                            this_arrangement = [0] * 8
                            for i in range(8):
                                this_arrangement[i] = arrangement[perm[i]]
                            if test_arrangement(this_arrangement):
                                print("FUUUUCK", this_arrangement)
                                not_ended = False
                                break
                            print(j, " --- ", this_arrangement)
                            j += 1
